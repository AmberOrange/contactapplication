package se.experis;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Program {


    public static void main(String[] args) {
        // Get sqlite database locally from the resource folder
        String URL = "jdbc:sqlite::resource:contact_database.sqlite";
        Connection conn = null;
        ArrayList<Contact> contacts = new ArrayList<>();
        try {

            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            Scanner inputScanner = new Scanner(System.in);
            System.out.print("Enter a search term:\n>");
            String searchTerm = inputScanner.nextLine();

            // This constructs the query statement to be requested
            String stringStatement = "SELECT * FROM contacts WHERE ";
            stringStatement += String.format("full_name LIKE '%%%s%%' OR ", searchTerm);
            stringStatement += String.format("address LIKE '%%%s%%' OR ", searchTerm);
            stringStatement += String.format("mobile_number LIKE '%%%s%%' OR ", searchTerm);
            stringStatement += String.format("work_number LIKE '%%%s%%' OR ", searchTerm);
            stringStatement += String.format("birthdate LIKE '%%%s%%'", searchTerm);

            // For DEBUG purposes; prints the statement as it will be sent in
            //System.out.println(stringStatement);

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement(stringStatement);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                contacts.add(
                    new Contact(
                        resultSet.getString("full_name"),
                        resultSet.getString("address"),
                        resultSet.getString("mobile_number"),
                        resultSet.getInt("work_number"),
                        resultSet.getString("birthdate")
                    ));
            }

            // Print if no contacts was found
            if(contacts.isEmpty()) {
                System.out.println("No matches were found.");
            } else {
                // Print a header
                System.out.printf("%-26s%-35s%-15s%-10s%-10s%n", "Full Name", "Address", "Phone Number", "Work Nr.", "Birthdate");
                // Print results
                for (Contact contact :
                        contacts) {
                    contact.printAllInfoFormatted(searchTerm);
                }
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
    }
}
