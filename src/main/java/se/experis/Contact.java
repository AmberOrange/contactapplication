package se.experis;

public class Contact {
    private final String fullName;
    private final String address;
    private final String mobileNumber;
    private final int workNumber;
    private final String birthdate;

    // Special symbols for turning on and off color in the console
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_COLOR = "\u001B[32m";

    public Contact(String fullName, String address, String mobileNumber, int workNumber, String birthdate) {
        this.fullName = fullName;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.workNumber = workNumber;
        this.birthdate = birthdate;
    }

    private String formatStringWithSearchColor(String string, String searchTerm) {
        // To be able to ignore cases, make the string and term into lower case
        String lowerCaseString = string.toLowerCase();
        String lowerCaseSearchTerm = searchTerm.toLowerCase();
        // Check if the string has the search term, and at what index
        int index;
        if((index = lowerCaseString.indexOf(lowerCaseSearchTerm)) != -1) {
            // Create a string builder and inject the color symbols surrounding the term
            StringBuilder stringBuilder = new StringBuilder(string);
            stringBuilder.insert(index, ANSI_COLOR);
            stringBuilder.insert(index + searchTerm.length() + ANSI_COLOR.length(),ANSI_RESET);
            // Return formatted string
            return stringBuilder.toString();
        } else {
            // Return normal string
            return string;
        }
    }

    // To ensure
    public void printAllInfoFormatted(String searchTerm) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;

        stringBuilder.append(formatStringWithSearchColor(fullName, searchTerm));
        for (i += fullName.length(); i <= 25; i++) {
            stringBuilder.append(' ');
        }

        stringBuilder.append(formatStringWithSearchColor(address, searchTerm));
        for (i += address.length(); i <= 60; i++) {
            stringBuilder.append(' ');
        }

        stringBuilder.append(formatStringWithSearchColor(mobileNumber, searchTerm));
        for (i += mobileNumber.length(); i <= 75; i++) {
            stringBuilder.append(' ');
        }

        String workNumberString = workNumber != 0 ? Integer.toString(workNumber) : "None";
        stringBuilder.append(formatStringWithSearchColor(workNumberString, searchTerm));
        for (i += workNumberString.length(); i <= 85; i++) {
            stringBuilder.append(' ');
        }
        stringBuilder.append(formatStringWithSearchColor(birthdate, searchTerm));
        System.out.println(stringBuilder.toString());
    }
}